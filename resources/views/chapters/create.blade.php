@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection


@section('content')
    <main class="Chapters  u-container  u-afterFixed">
        <header class="u-title">
            <h2>Capítulo nuevo de la serie {{ $serie->name }}</h2>
        </header>
        {!! Form::model(
            $chapter = new \App\Models\Chapter([
                'serie_id' => $serie->id
            ]),
            [
                'route' => ['admin.chapters.store', $serie]
            ]
        ) !!}
            @include('chapters.partials.form')
        {!! Form::close() !!}
    </main>
@endsection
