@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Series  u-container  u-afterFixed">
        <div class="u-fullScreen">
            {!! $serie->trailer_url !!}
        </div>
        <header class="u-title">
            <h2>{{ $serie->name }}</h2>
        </header>
        <p class="u-data">
            {{ $serie->information }}
        </p>
        <p class="u-data">
            Tags:
            @foreach($serie->tags as $tag)
                <span class="Label">{{ $tag->name }}</span>
            @endforeach
        </p>

        <header class="u-flex-space-between">
            <h2 class="u-data">{{ $serie->chapters->count() }} capítulos</h2>
            @if(Auth::user()->role_id === 1)
                <span class="u-text-right">
                    <a href="{{ route('admin.chapters.create', $serie) }}" class="u-button  u-bg-alert">Añadir capítulo</a>
                </span>
            @endif
        </header>

        @include('series.partials.list')
    </main>
@endsection
